variable "emails" {
  description = "Список email'ов для которых нужно сгенерировать пароль"
  type        = list(string)
}
