resource "random_string" "password_generator" {
  count   = length(var.emails)
  length  = 12
  special = true
  upper   = true
  lower   = true
  numeric = true
}
