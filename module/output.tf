output "email_password_pairs" {
  value = { 
    for idx, email in var.emails : email => random_string.password_generator[idx].result 
  }
}
