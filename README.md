# Email Password Generator

Простой модуль для генерации паролей для входящего списка email адресов.


Использован ресурc random_string для генерации пароля вместо random_password для упрощения вывода.

## Команды для запуска

Инициализация 
```terraform init```

План 
```terraform plan```

Применение
```terraform apply```
